__start__: obj interp __plugin__
	export LD_LIBRARY_PATH="./libs"; ./interp

obj:
	mkdir obj

__plugin__:
	cd plugin; make

CPPFLAGS=-Wall -pedantic -Iinc -std=c++11 -g
LDFLAGS=-Wall

interp: obj/main.o obj/plugin.o obj/xmlparser4scene.o obj/obsluga_xml.o
	g++ ${LDFLAGS} -o interp obj/main.o obj/plugin.o obj/xmlparser4scene.o obj/obsluga_xml.o -ldl -lreadline -lxerces-c

obj/main.o: src/main.cpp inc/command.hh inc/plugin.hh inc/robot.hh inc/obsluga_xml.hh inc/scene.hh
	g++ -c ${CPPFLAGS} -o obj/main.o src/main.cpp

obj/plugin.o: src/plugin.cpp inc/plugin.hh inc/command.hh inc/scene.hh
	g++ -c ${CPPFLAGS} -o obj/plugin.o src/plugin.cpp

obj/xmlparser4scene.o: src/xmlparser4scene.cpp inc/xmlparser4scene.hh
	g++ -c ${CPPFLAGS} -o obj/xmlparser4scene.o src/xmlparser4scene.cpp

obj/obsluga_xml.o: src/obsluga_xml.cpp inc/obsluga_xml.hh inc/scene.hh inc/xmlparser4scene.hh
	g++ -c ${CPPFLAGS} -o obj/obsluga_xml.o src/obsluga_xml.cpp

clean:
	rm -f obj/* interp core*


clean_plugin:
	cd plugin; make clean

cleanall: clean
	cd plugin; make cleanall
	cd dox; make cleanall
	rm -f libs/*
	find . -name \*~ -print -exec rm {} \;

help:
	@echo
	@echo "  Lista podcelow dla polecenia make"
	@echo 
	@echo "        - (wywolanie bez specyfikacji celu) wymusza"
	@echo "          kompilacje i uruchomienie programu."
	@echo "  clean    - usuwa produkty kompilacji oraz program"
	@echo "  clean_plugin - usuwa plugin"
	@echo "  cleanall - wykonuje wszystkie operacje dla podcelu clean oraz clean_plugin"
	@echo "             oprocz tego usuwa wszystkie kopie (pliki, ktorych nazwa "
	@echo "             konczy sie znakiem ~)."
	@echo "  help  - wyswietla niniejszy komunikat"
	@echo
	@echo " Przykladowe wywolania dla poszczegolnych wariantow. "
	@echo "  make           # kompilacja i uruchomienie programu."
	@echo "  make clean     # usuwa produkty kompilacji."
	@echo

doc:
	cd dox; make
