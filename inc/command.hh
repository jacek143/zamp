#ifndef  COMMAND_HH
#define  COMMAND_HH

#include <iostream>
#include "scene.hh"

/*!
 * @file command.hh
 *
 * @brief Definicja klasy Command
 *
 * Plik zawiera definicję klasy Command, która medeluje polecenie
 * dla robota.
 */

/*!
 * @brief Modeluje abstrakcyjne polecenie dla robota mobilnego
 *
 * Klasa modeluje polecenie dla robota mobilnego. Jest to klasa 
 * czysto wirtualna.
 */
 class Command {
  public:
   /*!
    * @brief Destruktor wirtualny ze względu na klasy pochodne.  
    */
   virtual ~Command() {}
   /*!
    * @brief Wyświetla nazwę polecenia i wartości jego parametrów.
    */
   virtual void PrintCmd() const = 0;
   /*!
    * @brief Wyświelta składnię polecenia.
    */
   virtual void PrintSyntax() const = 0;
   /*!
    * @brief Zwraca nazwę polecenia.
    *
    * @retval Nazwa polecenia.
    */
   virtual const char* GetCmdName() const = 0;
   /*!
    * @brief Wykonuje polecenie.
    *
    * Wykonuje polecenie na obiekcie modelującym pozycję robota
    * z wykorzystaniem wcześniej wczytanych parametrów.
    *
    * @param[in,out] scena - Referencja na obiekt przechowując
    * informacje o scenie, na której znajduje się robot oraz
    * inne obiekty.
    *
    * @retval Czy wykonanie polecenia skończyło się powodzeniem.
    */
   virtual bool ExecCmd( Scene & scena) const = 0;
   /*!
    * @brief Wczytuje parametry polecenia.
    *
    * @param[in] Strm_CmdsList - Strumień, w którym znajdują się 
    * parametry dla polecenia.
    *
    * @retval Czy operacja wczytania prametrów powiodła się.
    */
   virtual bool ReadParams(std::istream& Strm_CmdsList) = 0;
 };

#endif
