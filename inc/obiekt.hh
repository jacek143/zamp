#ifndef OBIEKT_HH
#define OBIEKT_HH

#include <string>

/**
 * @brief Klasa modeluje obiekt znajdujący się na scenie.
 * 
 * Klasa modeluje obiekt znajdujący się na scenie. Zawiera 
 * inforamcje na temat jego położenia, rozmiaru oraz nazwy.
 */
class Obiekt{
public:
  Obiekt(double x, double y, unsigned r, std::string nazwa) 
    : _x(x),_y(y), _r(r), _nazwa(nazwa){
    ;
  }
  /**
   * @brief Zwraca współrzędną x obiektu.
   *
   * Zwraca współrzędną x obiekty.
   *
   * @retval Współrzędna x obiektu.
   */
  double x(void) const{
    return _x;
  }
  /**
   * @brief Zwraca współrzędną y obiektu.
   *
   * Zwraca współrzędną y obiektu.
   *
   * @retval Współrzędna y obiektu.
   */
  double y(void) const{
    return _y;
  }
  /**
   * @brief Zwraca promień obiektu.
   *
   * Zwraca promień obiektu.
   *
   * @retval Promień obiektu.
   */
  unsigned r(void) const{
    return _r;
  }
  /**
   * @brief Zwraca nazwę obiektu.
   *
   * Zwraca nazwę obiektu.
   *
   * @retval Nazwa obiektu.
   */
  std::string nazwa(void) const{
    return _nazwa;
  }
  /**
   * @brief Ustawia nową wartość współrzędnej x.
   *
   * param[in] x - Nowa wartość współrzędnej x.
   */
  void x(double nowy){
    _x = nowy;
  }
  /**
   * @brief Ustawia nową wartość współrzędnej y.
   *
   * param[in] y - Nowa wartość współrzędnej y.
   */
  void y(double nowy){
    _y = nowy;
  }
  /**
   * @brief Ustawia nową wartość promienia.
   *
   * @param[in] r - Nowa wartość promienia.
   */
  void r(unsigned nowy){
    _r = nowy;
  }
  /**
   * @brief Ustawia nową nazwę obiektu.
   *
   * @param[in] nowy - Nowa nazwa obiektu.
   */
  void nazwa(std::string nowy){
    _nazwa = nowy;
  }
private:
  /**
   * @brief Współrzędna x obiektu.
   */
  double _x;
  /**
   * @brief Współrzędna y obiektu.
   */
  double _y;
  /**
   * @brief Promień obiektu.
   */
  unsigned _r;
  /**
   * @brief Nazwa obiektu.
   */
  std::string _nazwa;
};

#endif
