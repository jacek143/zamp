#ifndef PLUGIN_HH
#define PLUGIN_HH

/**
 * @file plugin.hh
 *
 * @brief Definicja klasy modelującej plugin.
 */

#include <string>
#include "command.hh"
#include <dlfcn.h>

/**
 * @brief Klasa modelująca plugin.
 *
 * Klasa odpowiada za prawidłowe załadowanie oraz usunięcie pluginu.
 * Przechowuje wskaźniki do załadowanej biblioteki oraz obiektu klasy będącej
 * pochodną klasy #Command.
 */
class Plugin{

public:

  /**
   * @brief Konstruktor.
   * 
   * Ustawia wszystkie wewnętrzne wskaźniki na nullptr.
   */
  Plugin(void);

  /**
   * @brief Konstruktor kopiujący.
   *
   * Dla nowego obiektu otwiera ponownie bibliotekę i tworzy nowy 
   * nową instancję obiektu klasy pochodnej dla #Command.
   *
   * Ponowne otwarcie biblioteki nie powoduje zwiększenia zużycia
   * pamięci, tylko inkrementację odpowiednich licznków. Jest to 
   * dokładnie opisane w dokumentacji biblioteki "dlfcn.h".
   *
   * @param[in] kopiowany - Kopiowany plugin.
   */
  Plugin(const Plugin & kopiowany);
  
  /**
   * @brief Destruktor.
   *
   * Usuwa z obiekty klasy pochodnej od klasy #Command i potem
   * zamyka bibliotekę.
   */
  ~Plugin();

  /**
   * @brief Operator przypisania.
   *
   * Robi to samo co konstruktor kopiujący.
   *
   * @param[in] kopiowany - Kopiowany plugin.
   *
   * @retval Referencja do tego obiektu.
   */
  Plugin & operator = (const Plugin & kopiowany);

  /**
   * @brief Ładowanie pluginu.
   *
   * Funkcja otwiera wskazaną bibliotekę i ładuje zapisany w niej 
   * plugin. Jeśli do obietku była już załadowana jakaś komenda, to
   * obiekt jest czyszczony.
   * 
   * @param[in] library_name - nazwa biblioteki, w której znajduje 
   * się plugin.
   *
   * @retval Czy załadowanie pluginu skończyło się powowdzeniem.
   */
  bool load(std::string library_name);

  /**
   * @brief Dostęp do nazwy wczytanej komendy.
   *
   * Realizuje dostępn do nazwy komendy (obiekt klasy pochodnej od
   * #Command), do której wskaźnki przechowuje ta klasa.
   *
   * @param[out] command_name - Miejsce na zapisanie nazwy komendy.
   *
   * @retval Jeśli obiekt nie zawiera komendy, to zostanie zwrócony
   * false.
   */
  bool get_command_name(std::string & command_name) const;

  /**
   * @brief Wypisuje składnię przechowywanej komendy.
   *
   * @retval Jeśli obiekt nie zawiera komendy, to zostanie zwrócony
   * false.
   */
  bool print_command_syntax(void) const;

  /**
   * @brief Czyszczenie pluginu.
   *
   * Usuwa z obiekty z pamięci i zamyka biblioteki w odpowiedniej 
   * kolejności.
   */
  void clear(void);

  /**
   * @brief Wczytanie parametrów komendy.
   *
   * @param[in] strm_cmd_list - Strumień, z którego zostaną 
   * wczytane parametry.
   *
   * @retval Czy wczytane parametry są poprawne.
   */
  bool read_params(std::istream & strm_cmd_list);

  /**
   * @brief Wykonuje komendę na podanym obiekcie
   *
   * Próbuje wykonać komendę, na którą wskazuje Plugin#_p_command
   * na obiekcie modelującym pozycję robota. W razie 
   * niepowodzenia jest wyświetlany komunikat o błędzie.
   *
   * @param[out,in] scena - Referencja na obiekt przechowujący
   * informacje o scenie.
   *
   * @retval Czy operacja się udała.
   */
  bool exec_cmd(Scene & scena) const;

private:

  /**
   * @brief Wskaźnik na bibliotekę, z której korzysta dany plugin.
   */
  void * _p_library_handler;

  /**
   * @brief Wskaźnik na obiekt komendy.
   */
  Command * _p_command;

  /**
   * @brief Nazwa załadowanej biblioteki.
   */
  std::string _library_name;
  

};

#endif
