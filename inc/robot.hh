#ifndef ROBOT_HH
#define ROBOT_HH

/**
 * @file robot.hh
 *
 * @author Jacek Jankowski
 *
 * @date 15.11.2015
 *
 * @brief Plik z klasą modelującą robota.
 */

#include "robotpose.hh"

/**
 * @brief Klasa modelująca robota.
 *
 * Klasa zawiera informacje o stanie robota.
 *
 */
class Robot : public RobotPose {
protected:
   /*!
    * @brief Wywoływana zawsze po aktualizacji położenia i orientacji
    *  lub samego położenia.
    *
    *  Metoda wywoływana jest po aktualizacji położenia i orientacji
    *  lub samego położenia.
    * 
    * Funkcja, która może umożliwić w przyszłości komunikację z GnuPlotem.
    *
    *  \retval true - gdy operacja powiodła się,
    *  \retval false - w przypadku przeciwnym.
    */
  virtual bool AfterUpdate(){
    return true;
  }
};

#endif
