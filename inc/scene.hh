#ifndef SCENE_HH
#define SCENE_HH

#include "obiekt.hh"
#include <list>
#include "robot.hh"

/*!
 * \file scene.hh
 *
 * \brief Plik z klasą modelującą scenę.
 *
 * Plik z klasą modelującą scenę.
 */

/*!
 * \brief Klasa modelująca scenę.
 *
 * Klasa modeluje scenę, na której znajduje się robot i obiekty.
 */
class Scene {
public:
  /**
   * @brief Lista obiektów znajdujących się na schenie.
   */
  std::list<Obiekt> _lista_obiektow;
  
  /**
   * @brief Robot, który znajduje się na scenie.
   */
  Robot _robot;
};


#endif
