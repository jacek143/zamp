#ifndef  COMMAND4GRASPER_HH
#define  COMMAND4GRASPER_HH

#ifndef __GNUG__
# pragma interface
# pragma implementation
#endif

#include "command.hh"
#include <cmath>

/*!
 * @file command4grasper.hh
 * 
 * @brief Definicja klasy Command4Grasper.
 *
 * Zawiera definicję polecenia implementującego 
 * chwycenie/upuszczenie przedmiotu.
 */

/*!
 * @brief Modeluje polecenia implementujące chwycenie/upuszczenie 
 * przedmiotu.
 */
class Command4Grasper: public Command {

  /**
   * @brief Polecenie dla chwytaka - chwycić czy upuścić.
   *
   * 1 oznacza, że chwytak ma złapać przedmiot.
   * 0 oznacza, że chwytak ma upuścić trzymany przedmiot.
   */
  bool _chwytac;
 
public:

  /*!
   * \brief Wyświetla nazwę polecenia i wartości jego parametrów.
   */
  virtual void PrintCmd() const;

  /*!
   * \brief Wyświetla składnię polecenia.
   */
  virtual void PrintSyntax() const;

  /*!
   * \brief Zwraca nazwę polecenia.
   */
  virtual const char* GetCmdName() const;

  /*!
   * @brief Chwycenie/upuszczenie obiektu.
   *
   * Robot próbuje chwycić/upuścić przedmiot w zależności od 
   * zawartości zmiennej command4grasper#_chwytac.
   * Jeśli każemy upuścić/chwycić, kiedy trzymamy nic/coś, to 
   * polecenie nie zostanie wykonane i zostanie wyświetlone 
   * ostrzeżenie.
   *
   * Upuszczonemu obiektowi zostaną nadane współrzędne robota z 
   * chwili upuszczania.
   *
   * Przedmiot zostanie chwycony tylko wtedy, gdy robot będzie 
   * znajdował się w odległości równej lub mniejszej niż 
   * (1+promień_obiektu).
   *
   * @param[in,out] scena - Referencja na obiekt przechowując
   * informacje o scnenie, na której znajduje się robot oraz
   * inne obiekty.
   *
   * @retval Czy operacja skończyła się powodzeniem.
   */
  virtual bool ExecCmd(Scene & scena) const;

  /*!
   * @brief Wczytywanie parametrów.
   *
   * Wczytuje parametry ze strumienia wejściowego.
   * Jeśli strumień będzie pusty/zepsuty 
   * lub  parametr będzie nieprawidłowy, to zostanie zwrócony fałsz.
   *
   * Wczytywany jest tylko jeden parametr. Jeśli będzie równy true,
   * to znaczy, że robot ma chwycić. Jeśli 0, to znaczy, że robot
   * ma upuścić trzymany przedmiot.
   *
   * @param[in] Strm_CmdsList - strumień, w którym znajdują się 
   * parametry.
   *
   * @retval Czy udało się wczytać prawidłowe parametry.
   */
  virtual bool ReadParams(std::istream& Strm_CmdsList);

  /*!
   * @brief Stworzenie nowego obiektu klasy Command4Grasper
   *
   * Tworzy nowy obiekt klasy Command4Grasper i zwraca wskaźnik na 
   * niego.
   *
   * @retval Wskaźnik na obiekt klasy Command4Grasper.
   */
  static Command* CreateCmd();
};

#endif
