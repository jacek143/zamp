#ifndef  COMMAND4MOVE_HH
#define  COMMAND4MOVE_HH

#ifndef __GNUG__
# pragma interface
# pragma implementation
#endif

#include "command.hh"
#include <cmath>

/*!
 * @file command4move.hh
 * 
 * @brief Definicja klasy Command4Move.
 *
 * Zawiera definicję polecenia implementującego ruch po lini prostej z zadaną 
 * prędkością.
 */

/*!
 * @brief Modeluje polecenia implementujące ruch po lini prostej z zadaną 
 * prędkością.
 */
class Command4Move: public Command {

  /**
   * @brief Prędkość liniowa.
   *
   * Prędkość z jaką ma się poruszać robot po lini prostej.
   */
  double  _speed_mm_per_s;

  /**
   * @brief Dystans.
   *
   * Dystans jaki ma przebyś robot w lini prostej.
   */
  double _distance_mm;
 
public:

  /*!
   * @brief Konstruktor polecenia.
   *
   * Konstruktor polecenia. Wszystkie parametry są ustawiane na 0;
   */
  Command4Move();  

  /*!
   * \brief Wyświetla nazwę polecenia i wartości jego parametrów.
   */
  virtual void PrintCmd() const;

  /*!
   * \brief Wyświetla składnię polecenia.
   */
  virtual void PrintSyntax() const;

  /*!
   * \brief Zwraca nazwę polecenia.
   */
  virtual const char* GetCmdName() const;

  /*!
   * @brief Wykonanie ruchu do przodu.
   *
   * Wykonuje ruch do przodu modyfikując obiekt przechowujący informacje o 
   * położeniu robota.
   *
   * @param[in,out] scena - Referencja na obiekt przechowując
   * informacje o scnenie, na której znajduje się robot oraz
   * inne obiekty.
   *
   * @retval Czy operacja skończyła się powodzeniem.
   */
  virtual bool ExecCmd(Scene & scena) const;

  /*!
   * @brief Wczytywanie parametrów.
   *
   * Wczytuje parametry ze strumienia wejściowego.
   * Jeśli strumień będzie pusty/zepsuty 
   * lub  parametr będzie nieprawidłowy, to zostanie zwrócony fałsz.
   *
   * Najpierw jest wczytywany parametr speed, który musi być niezerowy.
   * Następny jest parametr distance, który musi być liczbą dodatnią.
   *
   * @param[in] Strm_CmdsList - strumień, w którym znajdują się 
   * parametry.
   *
   * @retval Czy udało się wczytać prawidłowe parametry.
   */
  virtual bool ReadParams(std::istream& Strm_CmdsList);

  /*!
   * @brief Stworzenie nowego obiektu klasy Command4Move
   *
   * Tworzy nowy obiekt klasy Command4Move i zwraca wskaźnik na niego.
   *
   * @retval Wskaźnik na obiekt klasy Command4Move.
   */
  static Command* CreateCmd();
};

#endif
