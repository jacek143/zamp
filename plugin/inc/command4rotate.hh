#ifndef  COMMAND4ROTATE_HH
#define  COMMAND4ROTATE_HH

#ifndef __GNUG__
# pragma interface
# pragma implementation
#endif

#include "command.hh"

/**
 * @file
 * 
 * @brief Definicja klasy Command4Rotate
 */

/**
 * @brief Modeluje polecenie obrotu wokół własnej osi z zadaną 
 * prędkością kątową.
 */
class Command4Rotate: public Command {

  /**
   * @brief Kąt, o który ma się obrócić robot wokół własnej osi.
   */
  double  _ang_deg;
  
  /**
   * @brief Prędkość z jaką robot ma wykonać obrót.
   */
  double _ang_speed_deg_per_s;

public:

  /**
   * @brief Konstruktor polecenia.
   *
   * Konstruktor polecenia. Wszystkie parametry są ustawiane na 0;
   */
  Command4Rotate();

  /**
   * @brief Wyświetla nazwę polecenia i wartości jego parametrów.
   */
  virtual void PrintCmd() const;

  /**
   * @brief Wyświetla składnię polecenia.
   */
  virtual void PrintSyntax() const;

  /**
   * @brief Zwraca nazwę polecenia.
   * 
   * retval Nazwa polecenia "Rotate".
   */
  virtual const char* GetCmdName() const;

  /**
   * @brief Wykonuje obrót modyfikując obiekt przechowujący dane
   * o położeniu robota.
   *
   * @param[in,out] scena - Referencja na obiekt przechowując
   * informacje o scnenie, na której znajduje się robot oraz
   * inne obiekty.
   *
   * @retval Czy wykonanie obrotu skończyło się powodzeniem.
   */
  virtual bool ExecCmd(Scene & scena) const;

  /**
   * @brief Wczytywanie parametrów.
   *
   * Wczytuje parametry ze strumienia wejściowego.
   * Jeśli strumień będzie pusty/zepsuty lub  parametr będzie 
   * nieprawidłowy, to zostanie zwrócony fałsz.
   *
   * Najpierw jest wczytywany parametr ang_speed, który musi być 
   * niezerowy.
   * 
   * Następny jest parametr ang, który musi być liczbą dodatnią.
   *
   * @param[in] Strm_CmdsList - Strumień, w którym są zapisane 
   * parametry.
   *
   * @retval Czy udało się wczytać prawidłowe parametry.
   */
  virtual bool ReadParams(std::istream& Strm_CmdsList);
  
  /**
   * @brief Stworzenie nowego obiektu klasy Command4Rotate
   *
   * Tworzy nowy obiekt klasy Command4Rotate i zwraca wskaźnik na 
   * niego.
   *
   * @retval Wskaźnik na obiekt klasy Command4Rotate.
   */
  static Command* CreateCmd();
};

#endif
