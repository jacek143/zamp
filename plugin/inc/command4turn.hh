#ifndef  COMMAND4TURN_HH
#define  COMMAND4TURN_HH

#ifndef __GNUG__
# pragma interface
# pragma implementation
#endif

#include "command.hh"
#include <cmath>

/**
 * @file
 * @brief Definicja klasy Command4Turn
 */

/**
 * @brief Modeluje polecenie dla robota mobilnego,
 * które wymusza jego skręt po łuku o zadanym promieniu.
 */
class Command4Turn: public Command {

public:

  /**
   * @brief Konstruktor polecenia.
   *
   * Konstruktor polecenia. Wszystkie parametry są ustawiane na 0;
   */
  Command4Turn();  

  /**
   * @brief Wyświetla nazwę polecenia i wartości jego parametrów.
   */
  virtual void PrintCmd() const;
  
  /**
   * @brief Wyświetla składnię polecenia.
   */
  virtual void PrintSyntax() const;

  /**
   * @brief Zwraca nazwę polecenia.
   *
   * @retval Nazwa polecenia.
   */
  virtual const char* GetCmdName() const;

  /**
   * @brief Implementacja skrętu.
   *
   * Wykonuje skręt modyfikując obiekt zawierający informacjie o 
   * położeniu robota.
   *
   * @param[in,out] scena - Referencja na obiekt przechowując
   * informacje o scnenie, na której znajduje się robot oraz
   * inne obiekty.
   *
   * @retval Czy operacja się udała.
   */
  virtual bool ExecCmd(Scene & scena) const;

  /*!
   * @brief Wczytywanie parametrów.
   *
   * Wczytuje parametry ze strumienia wejściowego.
   * Jeśli strumień będzie pusty/zepsuty 
   * lub  parametr będzie nieprawidłowy, to zostanie zwrócony fałsz.
   *
   * Najpierw jest wczytywany parametr speed, który musi być 
   * niezerowy.
   * Następny jest parametr distance, który musi być liczbą 
   * dodatnią.
   * Następny jest parametr radius, który musi być być niezerowy.
   *
   * @param[in] Strm_CmdsList - strumień, w którym znajdują się 
   * parametry polecenia.
   * 
   * @retval Czy udało się wczytać prawidłowe parametry.
   */
  virtual bool ReadParams(std::istream& Strm_CmdsList);

  /*!
   * @brief Stworzenie nowego obiektu klasy Command4Turn
   *
   * Tworzy nowy obiekt klasy Command4Turn i zwraca wskaźnik na
   * niego.
   *
   * @retval Wskaźnik na obiekt klasy Command4Turn.
   */
  static Command* CreateCmd();

private:

  /**
   * @brief Prędkość liniowa w mm na sekundę.
   */
  double  _speed_mm_per_s;

  /**
   * @brief Odległość krzywoliniowa jaką ma przebyć robot w mm.
   */
  double _distance_mm;

  /**
   * @brief Promień skrętu w mm.
   */
  double _radius_mm;

};

#endif
