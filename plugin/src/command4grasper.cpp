#include <iostream>
#include "command4grasper.hh"
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cerr;

/**
 * @brief Wskaźnik na chwycony przedmiot.
 *
 * Wskaźnik na obecnie chwycony przedmiot. Jeśli nie jest 
 * trzymany żaden przedmiot, to zmienna zawiera nullptr.
 */
Obiekt * p_obiekt = nullptr;

extern "C" {
  Command* CreateCmd(void);
}

/*!
 * @brief Stworzenie nowego obiektu klasy Command4Grasper
 *
 * Tworzy nowy obiekt klasy Command4Grasper
 * i zwraca wskaźnik na niego.
 *
 * @retval Wskaźnik na obiekt klasy Command4Grasper.
 */
Command* CreateCmd(void){
  return Command4Grasper::CreateCmd();
}

void Command4Grasper::PrintCmd() const
{
  cout << GetCmdName() << '\t' << _chwytac << endl;
}

const char* Command4Grasper::GetCmdName() const
{
  return "Grasper";
}

bool Command4Grasper::ExecCmd(Scene & scena) const {
  if(_chwytac == false){
    if(p_obiekt == nullptr){
      cerr << "Robot nie moze nic upuscic, bo nic nie trzyma" 
	   << endl;
      return false;
    }
    else{
      double x, y, a;
      scena._robot.Get(x, y, a);
      p_obiekt->x(x);
      p_obiekt->y(y);
      scena._lista_obiektow.push_back(*p_obiekt);
      delete p_obiekt;
      p_obiekt = nullptr;
      return true;
    }
  }

  if(p_obiekt != nullptr){
    cerr << "Robot nie moze chwycic, bo juz cos trzyma." << endl;
    return false;
  }

  double odl, x, y, a;
  auto it = scena._lista_obiektow.begin();

  scena._robot.Get(x, y, a);
  for(; it != scena._lista_obiektow.end(); it++){
    odl = sqrt(pow(x-it->x(),2) + pow(y-it->y(),2));
    if(odl <= (1+it->r())){
      break;
    } // if
  } // for
  if(it == scena._lista_obiektow.end()){
    cerr << "Robot nie moze nic zlapac, bo jest za daleko." << endl;
    //cerr << odl << endl;
    return false;
  }
  p_obiekt = new Obiekt(*it);
  scena._lista_obiektow.erase(it);
  
  return true;
}

bool Command4Grasper::ReadParams(std::istream& Strm_CmdsList)
{
  bool retval = true;
  bool tmp_chwytac;

  Strm_CmdsList >> tmp_chwytac;
 
  if(Strm_CmdsList.fail()){
    cerr << "Wczytanie parametrow dla polecenia " 
	 << GetCmdName() << " nie powiodlo sie" << endl;
    retval = false;
  }
  
  if(retval){
    _chwytac = tmp_chwytac;
  }

  return retval;
}

Command* Command4Grasper::CreateCmd()
{
  return new Command4Grasper();
}

void Command4Grasper::PrintSyntax() const
{
  cout << GetCmdName() 
       << '\t' << "grasp[0/1]" 
       << endl;
}
