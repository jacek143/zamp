#include <iostream>
#include "command4move.hh"
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cerr;

extern "C" {
  Command* CreateCmd(void);
}

/*!
 * @brief Stworzenie nowego obiektu klasy Command4Move
 *
 * Tworzy nowy obiekt klasy Command4Move i zwraca wskaźnik na niego.
 *
 * @retval Wskaźnik na obiekt klasy Command4Move.
 */
Command* CreateCmd(void)
{
  return Command4Move::CreateCmd();
}

Command4Move::Command4Move(): _speed_mm_per_s(0), _distance_mm(0)
{}

void Command4Move::PrintCmd() const
{
  cout << GetCmdName() 
       << '\t' << _speed_mm_per_s  
       << '\t' << _distance_mm 
       << endl;
}

const char* Command4Move::GetCmdName() const
{
  return "Move";
}

bool Command4Move::ExecCmd(Scene & scena) const
{
  double new_x_mm, new_y_mm, old_x_mm, old_y_mm, alpha_deg, alpha_rad;
  
  scena._robot.Get(old_x_mm, old_y_mm, alpha_deg);
  alpha_rad = (alpha_deg * M_PI) / 180;
  new_x_mm = old_x_mm + _distance_mm * cos(alpha_rad);
  new_y_mm = old_y_mm + _distance_mm * sin(alpha_rad); 

  scena._robot.Set(new_x_mm, new_y_mm);

  return true;
}

bool Command4Move::ReadParams(std::istream& Strm_CmdsList)
{
  bool retval = true;
  double tmp_speed;
  double tmp_distance;

  Strm_CmdsList >> tmp_speed >> tmp_distance;
 
  if(Strm_CmdsList.fail()){
    cerr << "Wczytanie parametrow dla polecenia " 
	 << GetCmdName() << " nie powiodlo sie" << endl;
    retval = false;
  }
  else if(tmp_distance < 0){
    cerr << "Nieprawidłowa wartość parametru distance[mm] " 
	 << "dla polecenia " << GetCmdName() 
	 << ". Parametr musi być liczbą nieujemną." << endl;
    retval = false;
  }
  
  if(retval){
    _speed_mm_per_s = tmp_speed;
    _distance_mm = tmp_distance;
  }

  return retval;
}

Command* Command4Move::CreateCmd()
{
  return new Command4Move();
}

void Command4Move::PrintSyntax() const
{
  cout << GetCmdName() 
       << '\t' << "speed[mm/s]"
       << '\t' << "distance[mm]" 
       << endl;
}
