#include <iostream>
#include "command4rotate.hh"
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cerr;

extern "C" {
  Command* CreateCmd(void);
}

/*!
 * @brief Stworzenie nowego obiektu klasy Command4Rotate
 *
 * Tworzy nowy obiekt klasy Command4Rotate i zwraca wskaźnik na 
 * niego.
 *
 * @retval Wskaźnik na obiekt klasy Command4Rotate.
 */
Command* CreateCmd(void)
{
  return Command4Rotate::CreateCmd();
}

Command4Rotate::Command4Rotate(): _ang_deg(0),
				  _ang_speed_deg_per_s(0)
{}

void Command4Rotate::PrintCmd() const
{
  cout << GetCmdName() 
       << " " << _ang_speed_deg_per_s 
       << " " << _ang_deg 
       << endl;
}

const char* Command4Rotate::GetCmdName() const
{
  return "Rotate";
}

bool Command4Rotate::ExecCmd(Scene & scena) const
{
  double old_x_mm, old_y_mm, old_alpha, new_alpha;
  
  scena._robot.Get(old_x_mm, old_y_mm, old_alpha);

  new_alpha = old_alpha;
  if(_ang_deg == 0 or _ang_speed_deg_per_s == 0){
    new_alpha += 0;
  }
  else if(_ang_speed_deg_per_s < 0){
    new_alpha -= _ang_deg;
  }
  else{
    new_alpha += _ang_deg;
  }

  scena._robot.SetAlpha(new_alpha);

  return true;
}

bool Command4Rotate::ReadParams(std::istream& Strm_CmdsList)
{
    bool retval = true;
  double tmp_ang_speed;
  double tmp_ang;

  Strm_CmdsList >> tmp_ang_speed >> tmp_ang;
 
  if(Strm_CmdsList.fail()){
    cerr << "Wczytanie parametrow dla polecenia " 
	 << GetCmdName() << " nie powiodlo sie" << endl;
    retval = false;
  }
  else if(tmp_ang < 0){
    cerr << "Nieprawidłowa wartość parametru ang[degree] " 
	 << "dla polecenia " << GetCmdName() 
	 << ". Parametr musi być liczbą dodatnią." << endl;
    return false;
  }
  
  if(retval){
    _ang_speed_deg_per_s = tmp_ang_speed;
    _ang_deg = tmp_ang;
  }

  return retval;
}

Command* Command4Rotate::CreateCmd()
{
  return new Command4Rotate();
}

void Command4Rotate::PrintSyntax() const
{
  cout << GetCmdName() 
       << '\t' << "speed[deg/s]" 
       << '\t' << "ang[deg]" 
       << endl;
}
