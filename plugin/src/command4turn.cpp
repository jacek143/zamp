#include <iostream>
#include "command4turn.hh"
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cerr;

extern "C" {
  Command* CreateCmd(void);
}

/*!
 * @brief Stworzenie nowego obiektu klasy Command4Turn
 *
 * Tworzy nowy obiekt klasy Command4Turn i zwraca wskaźnik na niego.
 *
 * @retval Wskaźnik na obiekt klasy Command4Turn.
 */
Command* CreateCmd(void)
{
  return Command4Turn::CreateCmd();
}

Command4Turn::Command4Turn(): _speed_mm_per_s(0), _distance_mm(0), 
			      _radius_mm(0)
{}

void Command4Turn::PrintCmd() const
{
  cout << GetCmdName() 
       << '\t' << _speed_mm_per_s  
       << '\t' << _distance_mm
       << '\t' << _radius_mm
       << endl;
}

const char* Command4Turn::GetCmdName() const
{
  return "Turn";
}

bool Command4Turn::ExecCmd(Scene & scena) const
{
  double new_x, new_y, new_alpha;
  double old_x, old_y, old_alpha;
  double angle_rad = _distance_mm/_radius_mm;
 
  scena._robot.Get(old_x, old_y, old_alpha);
  
  double q[2][1] = {{sin(angle_rad)},{1-cos(angle_rad)}};
  q[0][0] *= _radius_mm;
  q[1][0] *= _radius_mm;
  
  double old_alpha_rad = (old_alpha*M_PI)/180;
  double R[2][2] = {{+cos(old_alpha_rad),-sin(old_alpha_rad)},
		    {+sin(old_alpha_rad),+cos(old_alpha_rad)}};

  new_x = old_x + R[0][0]*q[0][0] + R[0][1]*q[1][0];
  new_y = old_y + R[0][0]*q[0][0] + R[0][1]*q[1][0];
  new_alpha = old_alpha + (angle_rad*180)/M_PI;

  scena._robot.Set(new_x, new_y, new_alpha);

  return true;
}

bool Command4Turn::ReadParams(std::istream& Strm_CmdsList)
{
  bool retval = true;
  double tmp_speed;
  double tmp_distance;
  double tmp_radius;

  Strm_CmdsList >> tmp_speed >> tmp_distance >> tmp_radius;
 
  if(Strm_CmdsList.fail()){
    cerr << "Wczytanie parametrow dla polecenia " 
	 << GetCmdName() << " nie powiodlo sie" << endl;
    retval = false;
  }
  else if(tmp_speed == 0){
        cerr << "Nieprawidłowa wartość parametru speed[mm] " 
	     << "dla polecenia " << GetCmdName() 
	     << ". Parametr musi być liczbą różną od zera" << endl;
    return false;
  }
  else if(tmp_distance <= 0){
    cerr << "Nieprawidłowa wartość parametru distance[mm] " 
	 << "dla polecenia " << GetCmdName() 
	 << ". Parametr musi być liczbą dodatnią." << endl;
    return false;
  }
  else if(tmp_radius == 0){
        cerr << "Nieprawidłowa wartość parametru radius[mm] " 
	     << "dla polecenia " << GetCmdName() 
	     << ". Parametr musi być liczbą różną od zera" << endl;
    return false;
  }
  
  if(retval){
    _speed_mm_per_s = tmp_speed;
    _distance_mm = tmp_distance;
    _radius_mm = tmp_radius;
  }

  return retval;
}

Command* Command4Turn::CreateCmd()
{
  return new Command4Turn();
}

void Command4Turn::PrintSyntax() const
{
  cout << GetCmdName() 
       << '\t' << "speed[mm/s]"
       << '\t' << "distance[mm]" 
       << '\t' << "radius[mm]"
       << endl;
}
