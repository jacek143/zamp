/**
 * @file main.cpp
 *
 * @brief Plik zawierający główne funkcje programu.
 *
 * Plik zawiera najważniejsze funkcje, które statnowią szkielet 
 * programu.
 */

#include <iostream>
#include <dlfcn.h>
#include <cassert>
#include "command.hh"
#include "plugin.hh"
#include <string>
#include <sstream>
#include <cstdlib>
#include <readline/readline.h>
#include <readline/history.h>
#include <map>
#include <vector>
#include <list>
#include <stdio.h>
#include <sstream>
#include "robot.hh"
#include <fstream>
#include "obsluga_xml.hh"

using namespace std;

bool dodaj_wtyczke(map<string,Plugin> & plugin_map);
bool usun_wtyczke(map<string,Plugin> & plugin_map);
bool wczytaj_plik(stringstream & strumien_znakowy);
void wyswietl_menu();
void pokaz_dostepne_wtyczki(const std::map<std::string,Plugin> & plugin_map);
bool wyswietl_sekwencje(const stringstream & strumien_instrukcji);
bool wykonaj_sekwencje(Scene & scena, const stringstream & strumien_instrukcji, map<string,Plugin> & plugin_map);
bool wczytaj_domyslne_pluginy(map<string, Plugin> & plugin_map);

/**
 * @brief Wczytanie domyślnych pluginów, opisu sceny i obsługa menu.
 *
 * Wczytuje domyślne pluginy, opis sceny i odpytuje użytkownika o 
 * to co zrobić dalej. Po otrzymaniu odpowiedzi wywołuje odpowiednią
 * fukcję.
 * Jeśli nie uda się załadować domyślnych wtyczek, to program
 * kończy działanie.
 *
 * @retval 0 - Normalne zakończenie programu.
 *
 * @retval 1 - Błąd wczytania jednego z domyślnych pluginów lub 
 * opisu sceny.
 */
int main(){
  char Opcja = '?';
  char * sLiniaPolecenia;
  const char * sZacheta = "Twoj wybor (? - menu)> ";
  istringstream StrmWe;

  map<string, Plugin> plugin_map;
  stringstream strumien_instrukcji;

  Scene scena;

  if(not wczytaj_domyslne_pluginy(plugin_map)){
    cerr << "!!! Blad wczytania domyslnych pluginow !!!" << endl;
      return 1;
  }

  if(!ReadFile("opis_sceny.xml",scena)){
    cerr << "!!! Blad wczytania opisu sceny !!!" << endl;
    return 1;
  }

  rl_bind_key('\t', rl_complete);
  do{
    switch(Opcja){
    case 'w':
      wczytaj_plik(strumien_instrukcji);
      break;
    case 'p':
      wyswietl_sekwencje(strumien_instrukcji);
      break;
    case 'i':
      pokaz_dostepne_wtyczki(plugin_map);
      break;
    case 's':
      wykonaj_sekwencje(scena, strumien_instrukcji, plugin_map);
      break;
    case 'a':
      dodaj_wtyczke(plugin_map);
      break;
    case 'd':
      usun_wtyczke(plugin_map);
      break;
    case '?': wyswietl_menu(); break;
    }
    sLiniaPolecenia = readline(sZacheta);
    cout << endl;
    if(!sLiniaPolecenia) {
      return 0;
    }
    add_history(sLiniaPolecenia);
    StrmWe.str(sLiniaPolecenia);
    free(sLiniaPolecenia);
  }while(StrmWe >> Opcja, Opcja != 'k');

  return 0;
}

/**
 * @brief Wczytuje domyślne pluginy.
 *
 * Wcztuje 4 domyślne pluginy: Move, Rotate, Turn oraz Grasper.
 * Jeśli operacja skończy się niepowodzeniem, to wyświetla
 * komunikat o błędzie i zwraca false.
 *
 * @param[out] plugin_map - Referencja do mapy, w której zostaną 
 * umieszczone pluginy.
 *
 * @retval Czy operacja skończyła się powodzeniem.
 */
bool wczytaj_domyslne_pluginy(map<string, Plugin> & plugin_map){
  vector<string> library_names{"command4move.so", 
      "command4rotate.so", "command4turn.so", "command4grasper.so"};

  for(unsigned i = 0; i < library_names.size(); i++){
    Plugin plugin_tmp;
    string command_name;
    if(not plugin_tmp.load(library_names[i])){
      cerr << "!!! Zaladowanie biblioteki " << library_names[i] 
	   << " nie powiodlo sie." << endl;
      return false;
    } // if
    if(not plugin_tmp.get_command_name(command_name)){
      cerr << "!!! Wczytana komenda nie ma nazwy !!!" << endl;
      return false;
    }
    plugin_map[command_name] = plugin_tmp;
  }
  return true;
}

/**
 * @brief Wyświetlanie menu.
 *
 * Funkcja wyświetla menu na standardowym wyjściu.
 */
void wyswietl_menu(){
  cout << "w - wczytanie nazwy pliku sekwencji instrukcji " 
       << "dla robota" << endl
       << "p - pokaz sekwencje instrukcji" << endl
       << "i - pokaz dostepne instrukcje/wtyczki" << endl
       << "s - start wykonywania sekwencji instrukcji" << endl
       << "a - dodaj nowa wtyczke" << endl
       << "d - usun wtyczke" << endl
       << "? - Wyswietl Menu" << endl << endl
       << "k - Koniec" << endl << endl;
}

/**
 * @brief Wyświetla załadowane pluginy.
 *
 * Wyświetla składnię dostępnych poleceń udostępnianych przez 
 * wczytane pluginy.
 *
 * @param[in] plugin_map - Referencja do zbioru wykorzystywanych 
 * pluginów.
 */
void pokaz_dostepne_wtyczki(const std::map<string,Plugin> & plugin_map){
  for(map<string,Plugin>::const_iterator it = plugin_map.cbegin(); 
      it != plugin_map.cend(); it++){
    it->second.print_command_syntax();
  } // for
  cout << endl;
}

/**
 * @brief Usuwanie wtyczek.
 *
 * Odpytuje użytkownika o nazwę wtyczki i prubuje ją usunąć.
 * Jeśli wtyczka o podanej nazwie nie istnieje, to wyświetla
 * komunikat o błędzie i zwraca false.
 *
 * @param[in] plugin_map - Referencja do zbioru używanych wtyczek.
 *
 * @retval Czy operacja usuwania skończyła się powodzeniem.
 */
bool usun_wtyczke(map<string,Plugin> & plugin_map){
  string nazwa_wtyczki;
  bool retval = false;

  cout << "Podaj nazwe wtyczki: ";
  cin >> nazwa_wtyczki;
  cout << endl;
  
  map<string,Plugin>::iterator it = plugin_map.find(nazwa_wtyczki);
  if ((retval = (it != plugin_map.end()))){
    plugin_map.erase (it);
  }
  else{
    cerr << "Wtyczka o podanej nazwie nie istnieje" << endl << endl;
  }

  return retval;
}

/**
 * @brief Dodawanie wtyczek.
 *
 * Pyta użytkownika o nazwę biblioteki, próbuje załadować 
 * znajdującą się w niej wtyczkę i wyświetla komunikat o tym, czy 
 * operacja się udała.
 *
 * @param[in,out] plugin_map - Zbiór używanych pluginów.
 *
 * @retval Czy operacja dodawania zakończyła się powodzeniem.
 */
bool dodaj_wtyczke(map<string,Plugin> & plugin_map){
  Plugin plugin_tmp;
  string command_name, library_name;

  cout << "Podaj nazwe biblioteki: ";
  cin >> library_name;
  cout << endl;

  if(not plugin_tmp.load(library_name)){
    cerr << "!!! Zaladowanie biblioteki " << library_name 
	 << " nie powiodlo sie." << endl;
    return false;
  } // if
  if(not plugin_tmp.get_command_name(command_name)){
    cerr << "!!! Wczytana komenda nie ma nazwy !!!" << endl;
    return false;
  }
  plugin_map[command_name] = plugin_tmp;
  cout << "Zaladowano wtyczke " << command_name << endl << endl;
  return true;
}

/**
 * @brief Wykonanie sekwencji instrukcji.
 *
 * Modyfikuje pozycję robota według wczytanej sekwencji instrukcji.
 * Jeśli okaże się, że dana instrukcja jest nieznana lub ma 
 * nieprawidłowe parametry, to zostaje wyświetlony komunikat o 
 * błędzie i zwracana jest wartość false. 
 *
 * @param[in,out] scena - Referencja na obiekt przechowujący 
 * inforamcje o scenie, czyli o robocie i otaczających go obiektach.
 *
 * @param[in] strumien_instrukcji - Strumień tekstowy, w którym 
 * znajdują się instrukcje.
 *
 * @param[in] plugin_map - Zestaw używanych pluginów.
 *
 * @retval Czy wykonanie wszystkich instrukcji zakończyło się 
 * powodzeniem.
 */
bool wykonaj_sekwencje(Scene & scena, 
		       const stringstream & strumien_instrukcji, 
		       map<string,Plugin> & plugin_map){
  string nazwa;
  
  if(not strumien_instrukcji.good()){
    cerr << "!!! Strumien instrukcji jest uszkodzony !!!" << endl;
    return false;
  }

  stringstream strumien_wyj;
  stringstream strumien{strumien_instrukcji.str()};
  while((strumien >> nazwa).good()){
    auto it = plugin_map.find(nazwa);
    if(it == plugin_map.end()){
      cerr << "!!! Sekwencja instrukcji zawiera nieznane polecenie \"" << nazwa << "\". !!!" << endl;
      return false;
    }
    if(not it->second.read_params(strumien)){
      cerr << "!!! Sekwencja instrukcji zawiera bledny parametr dla polecenia \"" << nazwa << "\". !!!" << endl;
      return false;
    }
    if(not it->second.exec_cmd(scena)){
      cerr << "!!! Blad wykonania komendy \"" << nazwa << "\". !!!" << endl;
    }
    double x, y, a;
    scena._robot.Get(x, y, a);
    stringstream biezacy_wiersz;
    biezacy_wiersz << "Robot" << '(' << x << ',' << y << ',' << a 
		   << ')';
    for(auto it = scena._lista_obiektow.cbegin();
	it != scena._lista_obiektow.cend(); it++){
      biezacy_wiersz << '\t' << it->nazwa() << '(' << it->x() 
		     << ',' << it->y() << ',' << it->r() << ')';
    } 
    biezacy_wiersz << endl;
    strumien_wyj << biezacy_wiersz.str();
    cout << biezacy_wiersz.str();
  } // while
  cout << endl;

  cout << "Podaj nazwe pliku wyjsciowego: ";
  cin >> nazwa;
  cout << endl;
  ofstream plik(nazwa);
  if(not plik.is_open()){
    cerr << "!!! Blad otwarcia/utworzenia pliku \"" << nazwa << "\" do zapisu. !!!" << endl; 
    return false;
  }
  plik << strumien_wyj.str();
  plik.close();

  return true;
}



/**
 * @brief Wczytanie zawartości pliku do strumienia znakowego.
 *
 * Pobiera od użytkownika nazwę pliku, przepuszcza go przez
 * preprocesor i zapisuje komendy, ale bez sprawdzania ich 
 * poprawności.
 *
 * @param[out] strumien_znakowy - referencja do strumienia 
 * znakowego, w którym zostanie zapisana treść pliku. Przed 
 * wczyteniem pliku strumień zostanie zresetowany.
 *
 * @retval Czy operacja zakończyła się powodzeniem.
 */
bool wczytaj_plik(stringstream & strumien_znakowy){

  strumien_znakowy.str("");
  strumien_znakowy.clear();

  string nazwa_pliku;
  cout << "Podaj nazwe pliku: ";
  cin >> nazwa_pliku;
  cout << endl;

  string polecenie{"cpp -P "};
  polecenie += nazwa_pliku;

  FILE *pProc = popen(polecenie.c_str(), "r");
  if(not pProc){
    cerr << "!!! Blad przetwarzania pliku przez preprocesor !!!" 
	 << endl;
    return false;
  }

  char bufor[512];
  while(fgets(bufor, sizeof(bufor), pProc)){
    strumien_znakowy << bufor;
  }

  pclose(pProc);

  return true;
}

/**
 * @brief Wyświetlenie wczytanej sekwencji instrukcji.
 *
 * Wyświetla wcześniej wczytaną sekwencję instrukcji ze stumienia 
 * znakowego.
 *
 * @param[in] strumien_instrukcji - Strumień znakowy, w którym 
 * znajduje się sekwencja instrukcji dla robota.
 *
 * @retval Czy operacja skończyła się powodzeniem.
 */
bool wyswietl_sekwencje(const stringstream & strumien_instrukcji){
  bool retval = true;

  if(strumien_instrukcji.good()){
    cout << strumien_instrukcji.str() << endl;
  }
  else{
    retval = false;
  }

  if(retval == false){
    cerr << "!!! Blad wyswietlenia sekwencji instrukcji !!!" 
	 << endl;
  }

  return retval;
}
