/**
 * @file plugin.cpp
 *
 * @brief Definicje funkcji składowych klasy #Plugin
 * 
 * W pliku umiesczcono fukncjie składowe klasy #Plugin.
 */

#include "plugin.hh"

using namespace std;

bool Plugin::exec_cmd(Scene & scena) const{
  if(not _p_command or not _p_library_handler){
    cerr << "Do pluginu nie zaladowano jeszcze biblioteki !!!" << endl;
    return false;
  }
  return _p_command->ExecCmd(scena);
}

bool Plugin::get_command_name(std::string & command_name) const {
  if(_p_command){
    command_name = _p_command->GetCmdName();
    return true;
  }
  cerr << "!!! Do pluginu nie zaladowano jeszcze biblioteki !!!" << endl;
  return false;
}

bool Plugin::print_command_syntax(void) const{
  if(_p_command){
    _p_command->PrintSyntax();
    return true;
  }
  return false;
}

Plugin & Plugin::operator = (const Plugin & kopiowany){
  if(this != &kopiowany){
    clear();
    if(kopiowany._library_name != ""){
      this->load(kopiowany._library_name);
    }
  }
  return *this;
}

Plugin::Plugin(void) : _p_library_handler(nullptr), 
		       _p_command(nullptr), _library_name("") {}
void Plugin::clear(void){
  if(_p_command){
    delete _p_command;
  }
  if(_p_library_handler){
    dlclose(_p_library_handler);
  }
  _p_command = nullptr;
  _p_library_handler = nullptr;
  _library_name = "";
}

Plugin::~Plugin(){
  clear();
}

Plugin::Plugin(const Plugin & kopiowany){
  this->clear();
  if(kopiowany._library_name != ""){
    this->load(kopiowany._library_name);
  }
}

bool Plugin::load(std::string library_name){
  Command *(*pCreateCmd)(void);
  void *pFun;

  clear();

  _p_library_handler = dlopen(library_name.c_str(),RTLD_LAZY);
  if (not _p_library_handler) {
    cerr << "!!! Brak biblioteki: " << library_name << endl;
    return false;
  }


  pFun = dlsym(_p_library_handler,"CreateCmd");
  if (not pFun) {
    cerr << "!!! Nie znaleziono funkcji CreateCmd" << endl;
    clear();
    return false;
  }
  pCreateCmd = *reinterpret_cast<Command* (**)(void)>(&pFun);

  _p_command = pCreateCmd();

  if(not _p_command){
    clear();
    cerr << "!!! Funkcja CreateCmd zwróciła nieprawidłowy wskaźnik"
	 << endl;
    return false;
  }
  
  _library_name = library_name;

  return true;
}

bool Plugin::read_params(std::istream & strm_cmd_list){
  if(_p_command){
    return _p_command->ReadParams(strm_cmd_list);
  }
  else{
    return false;
  }
}
